import numpy as np
import pdb

class Perceptron:

    def __init__(self):
        pass

    def __predict(self, X):
        return np.sign(np.dot(self.weights_, np.transpose(X)))


    def fit(self, X, Y):

        #Adds the usual column of ones to support the bias
        X = np.hstack((np.ones((X.shape[0], 1)), X))

        #Initialize weights and predicted values to some random values (which is zero in this case)
        self.weights_ = np.zeros(X.shape[1])
        predict = np.ones(X.shape[0])

        #Perceptron Learning Algorithm
        while not np.array_equal(self.__predict(X), Y):
            index = np.random.randint(0, X.shape[0])
            predict[index] = np.sign(np.dot(self.weights_, X[index]))

            if predict[index] != Y[index]:
                self.weights_ += (Y[index] - predict[index]) * X[index]

        return

    def predict(self, X):
        X = np.hstack((np.ones((X.shape[0], 1)), X))

        return self.__predict(X)
